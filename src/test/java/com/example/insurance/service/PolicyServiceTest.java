package com.example.insurance.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import com.example.insurance.entity.Policy;
import com.example.insurance.repository.EmployeePolicyRepository;
import com.example.insurance.repository.PolicyRepository;


@SpringBootTest
public class PolicyServiceTest {
	
	@InjectMocks
	PolicyService policyservice;

	@Mock
	EmployeePolicyRepository employeepolicyrepository;
	
	@Mock
	PolicyRepository policyrepository;

	@Test
	public void Testfetchallpolicies() {
		
		Policy policy = new Policy();
		policy.setMaximumMaturityAge("20");
		policy.setMinEntryAge("25");
		policy.setMinimumPremium("100");
		policy.setMinPolicyTerm("10");
		policy.setMinSumAssured("100000");
		policy.setPolicyDesc("policy desc");
		policy.setPolicyId(1);
		policy.setPolicyName("lic policy");
		

		List<Policy> list = new ArrayList<>();
		list.add(policy);

		when(policyrepository.findAll()).thenReturn(list);

		List<Policy> result = (List<Policy>) policyservice.fetchallpolicies();

		assertEquals(1, result.size());
	}

}
