package com.example.insurance.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import java.util.List;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import com.example.insurance.entity.EmployeePolicy;
import com.example.insurance.entity.PolicyCount;
import com.example.insurance.repository.EmployeePolicyRepository;
import com.example.insurance.repository.PolicyRepository;

@SpringBootTest
public class EmployeePolicyServiceTest {
	
	@InjectMocks
	EmployeePolicyService employeepolicyservice;

	@Mock
	EmployeePolicyRepository employeepolicyrepository;
	
	@Mock
	PolicyRepository policyrepository;

	@Test
	public void TestselectPolicyForEmployee() {
		
		
		List<Integer> Ilist = new ArrayList<>();
		Ilist.add(2);
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		EmployeePolicy empol = new EmployeePolicy();
		for (int i = 0; i < Ilist.size(); i++) {

			int policyidvalue = Ilist.get(i);
			empol.setEmployeeId(1);
			empol.setPolicyId(policyidvalue);
			empol.setSubscribedOn(timeStamp);
			empol.setEmployeePolicyId(1);
			
		}
		EmployeePolicy result = employeepolicyservice.selectPolicyForEmployee(1, Ilist);
		assertEquals(1, result.getEmployeeId());
		assertEquals(2, result.getPolicyId());

}
	
	@Test
	public void Testgetpolicytrend() {
		
		
		List<PolicyCount> temlist = new ArrayList<PolicyCount>(10);
		PolicyCount pc = new PolicyCount();
		pc.setPolicyCountvr(1);
		pc.setPolicyId(3);
		temlist.add(pc);
		when(employeepolicyrepository.getpolicycount()).thenReturn(temlist);
		List<String> result1 = employeepolicyservice.getpolicytrend();
		
		
		long policypercent = (pc.getPolicyCountvr() * 100)/1;
		double policypercentd = (double)policypercent;
		String s1 = new String("Count and Percentage of policy number"+ pc.getPolicyId() + " respectively are " + pc.getPolicyCountvr() + " and " + policypercentd + "%");
		assertEquals(s1, result1.get(0));
		
		
		
}
	
	@Test
	public void Testgetpolicycurrenttrend() {
		
		List<Long> tlist = new ArrayList<>();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		EmployeePolicy empol = new EmployeePolicy();
		
		empol.setEmployeeId(1);
		empol.setPolicyId(4);
		empol.setSubscribedOn(timeStamp);
		empol.setEmployeePolicyId(1);
		
		List<EmployeePolicy> emplist = new ArrayList<EmployeePolicy>();
		emplist.add(empol);
		
		when(employeepolicyrepository.getcurpolicytrend()).thenReturn(emplist);
		tlist.add(empol.getPolicyId());
		List<Long> tempDistinct = tlist.stream().distinct().collect(Collectors.toList());
		
		List<String> result2 = employeepolicyservice.getpolicycurrenttrend();
		long curpolicypercent = (Collections.frequency(tlist, tempDistinct.get(0)) * 100)/1;
		double curpolicypercentd = (double)curpolicypercent;
		String s2 = new String("Count and Percentage of last Ten policies subscribed "+ empol.getPolicyId() + " respectively are " + Collections.frequency(tlist, empol.getPolicyId()) + " and " + curpolicypercentd + "%" );
		assertEquals(s2, result2.get(0));
	}
	
	
}
