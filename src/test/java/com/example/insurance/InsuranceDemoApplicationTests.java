package com.example.insurance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.insurance.entity.Policy;
import com.example.insurance.entity.SalientFeature;
import com.example.insurance.entity.TermsConditions;
import com.example.insurance.repository.EmployeePolicyRepository;
import com.example.insurance.repository.EmployeeRepository;
import com.example.insurance.repository.PolicyRepository;
import com.example.insurance.repository.SalientFeatureRepository;
import com.example.insurance.repository.TermsConditionsRepository;
import com.example.insurance.service.EmployeePolicyService;
import com.example.insurance.service.PolicyService;
import com.example.insurance.service.SalientFeatureService;
import com.example.insurance.service.TermsConditionService;

@SpringBootTest
class InsuranceDemoApplicationTests {
	
	@Mock
    PolicyRepository policyRepository;

 

    @InjectMocks
    PolicyService policyService;

 

    @InjectMocks
    SalientFeatureService salientFeatureService;

 

    @InjectMocks
    TermsConditionService termsConditionService;

 

    @Mock
    SalientFeatureRepository salientFeatureRepo;

 

    @Mock
    TermsConditionsRepository termsConditionsRepository;

 

    @Mock
    EmployeePolicyRepository employeePolicyRepository;
    
    @InjectMocks
    EmployeePolicyService  employeePolicyService;
    
    @Mock
    EmployeeRepository  employeeRepository;
    

	@Test
	void contextLoads() {
	}
	
	@BeforeEach
    public void init() {

 

    }
	
	@Test
    public void testgetPolicybyid() {
        PolicyService ps = new PolicyService();
        Long policyId = 1L;
        String policyName = "LIC jeevan Akshay";
        String minEntryAge = "18-25years";
        String maximumMaturityAge = "25 years";
        String minPolicyTerm = "150000";
        String minimumPremium = "1710";
        String minSumAssured = "150";
        String poilicyDesc = "At HCL, we want to develop portal which will be used by HCL employees internally so that they can select LIC policy";
        Policy policy = new Policy(policyId, policyName, minEntryAge, maximumMaturityAge, minPolicyTerm, minimumPremium,
                minSumAssured, poilicyDesc);
        ArrayList<Policy> list = new ArrayList<>();
        list.add(policy);
        System.out.println("ll" + list.toString());
        when(policyRepository.getById(1)).thenReturn(list);
        java.util.List<Policy> pp = policyService.fetchpolicybyid(policyId);
        System.out.println("pp:::::::::::::::" + pp.toString());
        System.out.println("later" + list.toString());
        assertEquals(1, pp.size());

 

    }

 

    @Test
    public void testSalientfeaturebyid() {
        Long salientFeatureId = 1L;
        Long policyId = 1L;
        String feature1 = "LIC jeevan Akshay";
        String feature5 = "1710";
        String feature3 = "150";
        String feature2 = "At HCL, we want to develop portal which will be used by HCL employees internally so that they can select LIC policy";

 

        SalientFeature sf = new SalientFeature(salientFeatureId, policyId, feature1, feature2, feature3, feature5);

 

        ArrayList<SalientFeature> list = new ArrayList();
        list.add(sf);
        System.out.println("ll" + list.toString());
        when(salientFeatureRepo.getSalientfeaturebyById(policyId)).thenReturn(list);
        java.util.List<SalientFeature> pp = salientFeatureService.getSalientfeaturebyid(policyId);
        System.out.println("pp:::::::::::::::" + pp.toString());
        System.out.println("later" + list.toString());
        assertEquals(1, pp.size());

 

    }

 

    @Test
    public void testtermsconditionsPolicybyid() {
        Long termsConditionId = 1L;
        Long policyId = 1L;
        String minEntryAge = "LIC jeevan Akshay";
        String maxEntryAge = "1710";
        String minPolicyTerm = "150";
        String minMaturityAge = "150";
        String maxPolicyTerm = "150";
        String maxMaturityAge = "150";
        String modesofPremium = "150";
        String taxBenefit = "150";
        String loanFacility = "150";
        String riders = "150";
        String gracePeriod = "150";
        String policyRevival = "150";
        TermsConditions tc = new TermsConditions(termsConditionId, policyId, minEntryAge, maxEntryAge, minPolicyTerm,
                maxPolicyTerm, minMaturityAge, maxMaturityAge, modesofPremium, taxBenefit, policyRevival, gracePeriod,
                riders, loanFacility);
        ArrayList<TermsConditions> list = new ArrayList();
        list.add(tc);
        System.out.println("ll" + list.toString());
        when(termsConditionsRepository.gettermsconditionsById(policyId)).thenReturn(list);
        java.util.List<TermsConditions> pp = termsConditionService.gettermsconditionsPolicybyid(policyId);
        System.out.println("pp:::::::::::::::" + pp.toString());
        System.out.println("later" + list.toString());
        assertEquals(1, pp.size());

 

    }
    
    
   
}
