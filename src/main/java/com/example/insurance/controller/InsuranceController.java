package com.example.insurance.controller;



import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.example.insurance.entity.EmployeePolicy;
import com.example.insurance.entity.Policy;
import com.example.insurance.entity.SalientFeature;
import com.example.insurance.entity.TermsConditions;
import com.example.insurance.service.EmployeePolicyService;
import com.example.insurance.service.PolicyService;
import com.example.insurance.service.SalientFeatureService;
import com.example.insurance.service.TermsConditionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/")
@Api(value = "InsuranceController")
public class InsuranceController {
	
	@Autowired
    private PolicyService policyService;

 

    @Autowired
    private SalientFeatureService salientFeatureService;

 

    @Autowired
    private TermsConditionService termsConditionService;

	
	
	@Autowired
	private EmployeePolicyService employeepolicyservice;
		
		
		@GetMapping("/policies")
		@ApiOperation("employee select a policy")
		public ResponseEntity<List<Policy>> getPolicies() {
			
			    return new ResponseEntity<>(policyService.fetchallpolicies(), HttpStatus.OK);
			
		}
		
		
		@PostMapping(path = "/policies/employeepolicy/{employeeId}")
		@ApiOperation("employee select a policy")
		private ResponseEntity<EmployeePolicy> selectPolicyForEmployee(@PathVariable("employeeId") long employeeId,
				@RequestBody List<Integer> policyList) {
			
			EmployeePolicy employeepolicy = employeepolicyservice.selectPolicyForEmployee(employeeId, policyList);
			return ResponseEntity.status(HttpStatus.OK).body(employeepolicy);
			
		}
		
		@GetMapping("/policies/overralltrend")
		@ApiOperation("To get Overrall Trend")
		public  List<String> getOverallTrend() {
			
			   
			return employeepolicyservice.getpolicytrend();
		}
		
		
		@GetMapping("/policies/currenttrend")
		@ApiOperation("To get Current Trend")
		public  List<String> getCurrentTrend() {
			
			   
			return employeepolicyservice.getpolicycurrenttrend();
		}
		
		@GetMapping("/policyid")

		 

	    @ApiOperation("fetch policy by id")
	    public ResponseEntity<List<Policy>> getPolicybyid(@RequestParam(value = "policyid") Long policyid) {
	        
	        return new ResponseEntity<>(policyService.fetchpolicybyid(policyid), HttpStatus.OK);
	    }

		@GetMapping("/policyid/salientFeature")

		 

	    @ApiOperation("fetch salient features policy by id")
	    public ResponseEntity<List<SalientFeature>> getSalientfeaturebyid(@RequestParam(value = "policyid") Long policyid) {
	        return new ResponseEntity<>(salientFeatureService.getSalientfeaturebyid(policyid),
	                HttpStatus.OK);
	    }
		
		@GetMapping("/policyid/termsconditions")

		 

	    @ApiOperation("fetch terms & conditions policy by id")
	    public ResponseEntity<List<TermsConditions>> gettermsconditionsPolicybyid(

	 

	            @RequestParam(value = "policyid") Long policyid) {

	 

	        return new ResponseEntity<>(termsConditionService.gettermsconditionsPolicybyid(policyid),
	                HttpStatus.OK);
	    }
}
