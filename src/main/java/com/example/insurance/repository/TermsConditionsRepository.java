package com.example.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.insurance.entity.TermsConditions;
@Repository
public interface TermsConditionsRepository extends JpaRepository<TermsConditions,Long> {

	
	@Query(value = "SELECT * from termsconditions t where t.policy_id=?1", nativeQuery = true)
    List<TermsConditions> gettermsconditionsById(Long policyid);
	

}

