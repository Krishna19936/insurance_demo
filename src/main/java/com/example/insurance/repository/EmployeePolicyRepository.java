package com.example.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.insurance.entity.EmployeePolicy;
import com.example.insurance.entity.PolicyCount;
@Repository
public interface EmployeePolicyRepository extends JpaRepository<EmployeePolicy,Long> {

	
	@Query("SELECT " + "new com.example.insurance.entity.PolicyCount(p.policyId, COUNT(p.policyId)) " +
	        "FROM EmployeePolicy p GROUP BY p.policyId")
	List<PolicyCount> getpolicycount();
	
	@Query(value="SELECT * FROM employee_policy order by subscribed_on DESC LIMIT 10",nativeQuery=true)
	List<EmployeePolicy> getcurpolicytrend();
	
	
	

	
	
	

}