package com.example.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.insurance.entity.SalientFeature;


@Repository
public interface SalientFeatureRepository extends JpaRepository<SalientFeature,Long> {

	
	@Query(value = "SELECT * from salientfeature where policy_id=?1", nativeQuery = true)
    List<SalientFeature> getSalientfeaturebyById(long policyid);
}
	


