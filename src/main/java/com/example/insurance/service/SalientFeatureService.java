package com.example.insurance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.insurance.entity.SalientFeature;
import com.example.insurance.repository.SalientFeatureRepository;

@Service
public class SalientFeatureService {
	
	@Autowired
    SalientFeatureRepository salientFeatureRepository;

	public List<SalientFeature> getSalientfeaturebyid(Long policyid) {

		 

        return salientFeatureRepository.getSalientfeaturebyById(policyid);
    }

}
