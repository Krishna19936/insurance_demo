package com.example.insurance.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.insurance.entity.EmployeePolicy;
import com.example.insurance.entity.PolicyCount;
import com.example.insurance.repository.EmployeePolicyRepository;



@Service
public class EmployeePolicyService {
	
	@Autowired
	private EmployeePolicyRepository employeepolicyRepository;
	
	
	
	
	public EmployeePolicy selectPolicyForEmployee(long employeeId, List<Integer> policylist ) {
		
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		EmployeePolicy employeepolicy = new EmployeePolicy();
		for (int i = 0; i < policylist.size(); i++) {

			int policyidvalue = policylist.get(i);
			employeepolicy.setEmployeeId(employeeId);
			employeepolicy.setPolicyId(policyidvalue);
			employeepolicy.setSubscribedOn(timeStamp);
			employeepolicyRepository.save(employeepolicy);
			
			
			
			
		}
		return employeepolicy;
		
	}
	
	public  List<String> getpolicytrend() {
		PolicyCount pl;
		List<PolicyCount> plist = employeepolicyRepository.getpolicycount();
		List<String> policyTrend = new ArrayList<>();
		 
		   
		double sum = 0;
		for (int i = 0; i < plist.size(); i++) {
			
			     pl =     plist.get(i);
			     sum = sum + pl.getPolicyCountvr();
			
		}
		
	if(sum == 0) {
			throw new NullPointerException();
		}
		
	     for (int i = 0; i < plist.size(); i++) {
			
			pl = plist.get(i);
			
			double policypercent = (pl.getPolicyCountvr() * 100)/sum;
			
			policyTrend.add("Count and Percentage of policy number"+ pl.getPolicyId() + " respectively are " + pl.getPolicyCountvr() + " and " + policypercent + "%" );
			
		}
		
		return policyTrend;
		
	}
	
	public List<String> getpolicycurrenttrend() {
		
		EmployeePolicy empol;
		List<EmployeePolicy> curlist = employeepolicyRepository.getcurpolicytrend();
		List<Long> templist = new ArrayList<>();
		
		List<String> currentPolicyTrend = new ArrayList<>();
		double sumcur = 0;
		
		for (int i = 0; i < curlist.size(); i++) {
			
			empol = curlist.get(i);
			templist.add(empol.getPolicyId());
			   
			}
		
		List<Long> tempDistinct = templist.stream().distinct().collect(Collectors.toList());
		
		for (int i = 0; i < tempDistinct.size(); i++) {
			
		    sumcur = sumcur + Collections.frequency(templist, tempDistinct.get(i));
			 
		}
		
		if(sumcur == 0) {
			throw new NullPointerException();
		}
			
		
		for (int i = 0; i < tempDistinct.size(); i++) {
			
			empol = curlist.get(i);
			double curpolicypercent = (Collections.frequency(templist, tempDistinct.get(i)) * 100)/sumcur;
			
			currentPolicyTrend.add("Count and Percentage of last Ten policies subscribed "+ empol.getPolicyId() + " respectively are " + Collections.frequency(templist, empol.getPolicyId()) + " and " + curpolicypercent + "%" );
			
		}
		
		return currentPolicyTrend;		
		
		
	}

}
