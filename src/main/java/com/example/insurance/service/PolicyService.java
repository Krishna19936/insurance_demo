package com.example.insurance.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.insurance.entity.Policy;
import com.example.insurance.repository.PolicyRepository;
import com.example.insurance.repository.SalientFeatureRepository;
import com.example.insurance.repository.TermsConditionsRepository;


@Service
public class PolicyService {
	
	@Autowired
	private PolicyRepository policyRepository;
	
	@Autowired
    private PolicyRepository policyrepo;

 

    @Autowired
    SalientFeatureRepository salientFeatureRepository;

 

    @Autowired
    TermsConditionsRepository termsConditionsRepository;

 
	
public List<Policy> fetchallpolicies() {
		
		return policyRepository.findAll();
		
		
	}

	
public List<Policy> fetchpolicybyid(Long policyid) {

	 

    return policyrepo.getById(policyid);
}



}

