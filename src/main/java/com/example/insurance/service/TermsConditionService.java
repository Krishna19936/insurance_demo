package com.example.insurance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.insurance.entity.TermsConditions;
import com.example.insurance.repository.TermsConditionsRepository;

@Service
public class TermsConditionService {
	
	@Autowired
    TermsConditionsRepository termsConditionsRepository;

	public List<TermsConditions> gettermsconditionsPolicybyid(Long policyid) {

		 

        return termsConditionsRepository.gettermsconditionsById(policyid);
    }

}
