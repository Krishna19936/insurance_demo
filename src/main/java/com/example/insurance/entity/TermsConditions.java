package com.example.insurance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "termsconditions")
public class TermsConditions {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "termsConditionId")
    private long termsConditionId;
    
    @Column(name = "policyId")
    private long policyId;
    
    @Column(name = "minEntryAge")
    private String minEntryAge;

 


    @Column(name = "maxEntryAge")
    private String maxEntryAge;
    
    @Column(name = "minPolicyTerm")
    private String minPolicyTerm;
    
    @Column(name = "maxPolicyTerm")
    private String maxPolicyTerm;
    
    @Column(name = "minMaturityAge")
    private String minMaturityAge;
    
    @Column(name = "maxMaturityAge")
    private String maxMaturityAge;
    
    @Column(name = "ModesofPremium")
    private String modesofPremium;
    
    public String getModesofPremium() {
		return modesofPremium;
	}



	public void setModesofPremium(String modesofPremium) {
		this.modesofPremium = modesofPremium;
	}



	@Column(name = "taxBenefit")
    private String taxBenefit;
    
    @Column(name = "policyRevival")
    private String policyRevival;
    
    @Column(name = "gracePeriod")
    private String gracePeriod;
    
    @Column(name = "riders")
    private String riders;
    
    
    

 

    public long getTermsConditionId() {
        return termsConditionId;
    }

 

    public void setTermsConditionId(long termsConditionId) {
        this.termsConditionId = termsConditionId;
    }

 

    public long getPolicyId() {
        return policyId;
    }

 

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }

 


    

 

    public String getTaxBenefit() {
        return taxBenefit;
    }

 

    public void setTaxBenefit(String taxBenefit) {
        this.taxBenefit = taxBenefit;
    }

 

    public String getPolicyRevival() {
        return policyRevival;
    }

 

    public void setPolicyRevival(String policyRevival) {
        this.policyRevival = policyRevival;
    }

 

    public String getGracePeriod() {
        return gracePeriod;
    }

 

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

 

    public String getRiders() {
        return riders;
    }

 

    public void setRiders(String riders) {
        this.riders = riders;
    }

 

    public String getLoanFacility() {
        return loanFacility;
    }

 

    public void setLoanFacility(String loanFacility) {
        this.loanFacility = loanFacility;
    }

 

    @Column(name = "loanFacility")
    private String loanFacility;

    @ManyToOne
	private Policy policy;
    
    
    public String getMinEntryAge() {
        return minEntryAge;
    }

 

    public void setMinEntryAge(String minEntryAge) {
        this.minEntryAge = minEntryAge;
    }

 

    public String getMaxEntryAge() {
        return maxEntryAge;
    }

 

    public void setMaxEntryAge(String maxEntryAge) {
        this.maxEntryAge = maxEntryAge;
    }

 

    public String getMinPolicyTerm() {
        return minPolicyTerm;
    }

 

    public void setMinPolicyTerm(String minPolicyTerm) {
        this.minPolicyTerm = minPolicyTerm;
    }

 

    public String getMaxPolicyTerm() {
        return maxPolicyTerm;
    }

 

    public void setMaxPolicyTerm(String maxPolicyTerm) {
        this.maxPolicyTerm = maxPolicyTerm;
    }

 

    public String getMinMaturityAge() {
        return minMaturityAge;
    }

 

    public void setMinMaturityAge(String minMaturityAge) {
        this.minMaturityAge = minMaturityAge;
    }

 

    public String getMaxMaturityAge() {
        return maxMaturityAge;
    }

 

    public void setMaxMaturityAge(String maxMaturityAge) {
        this.maxMaturityAge = maxMaturityAge;
    }

 

 

    public Policy getPolicy() {
        return policy;
    }

 

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

 

    public TermsConditions() {
        super();
    }

 

    public TermsConditions(long termsConditionId, long policyId, String minEntryAge, String maxEntryAge,
            String minPolicyTerm, String maxPolicyTerm, String minMaturityAge, String maxMaturityAge,
            String modesofPremium, String taxBenefit, String policyRevival, String gracePeriod, String riders,
             String loanFacility) {
        super();
        this.termsConditionId = termsConditionId;
        this.policyId = policyId;
        this.minEntryAge = minEntryAge;
        this.maxEntryAge = maxEntryAge;
        this.minPolicyTerm = minPolicyTerm;
        this.maxPolicyTerm = maxPolicyTerm;
        this.minMaturityAge = minMaturityAge;
        this.maxMaturityAge = maxMaturityAge;
        this.modesofPremium = modesofPremium;
        this.taxBenefit = taxBenefit;
        this.policyRevival = policyRevival;
        this.gracePeriod = gracePeriod;
        this.riders = riders;
        
        this.loanFacility = loanFacility;
    }
    
   
    

}
