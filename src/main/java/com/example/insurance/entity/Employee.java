package com.example.insurance.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employeeId")
    private long employeeId;
	
	@Column(name = "employeeName")
    private long employeeName;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	
	

	
	

	public Employee() {
		super();
	
	}

	public Employee(long employeeId, long employeeName) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
	}

	public long getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(long employeeName) {
		this.employeeName = employeeName;
	}

}
