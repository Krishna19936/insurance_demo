package com.example.insurance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "salientfeature")
public class SalientFeature {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "salientFeatureId")
    private long salientFeatureId;
	
	@Column(name = "policyId")
    private long policyId;
	
	@Column(name = "feature1")
    private String feature1;
	
	public SalientFeature() {
		super();
		
	}

	public SalientFeature(long salientFeatureId, long policyId, String feature1, String feature2, String feature3,
			String feature5) {
		super();
		this.salientFeatureId = salientFeatureId;
		this.policyId = policyId;
		this.feature1 = feature1;
		this.feature2 = feature2;
		this.feature3 = feature3;
		this.feature5 = feature5;
	}

	public long getSalientFeatureId() {
		return salientFeatureId;
	}

	public void setSalientFeatureId(long salientFeatureId) {
		this.salientFeatureId = salientFeatureId;
	}

	public long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}

	public String getFeature1() {
		return feature1;
	}

	public void setFeature1(String feature1) {
		this.feature1 = feature1;
	}

	public String getFeature2() {
		return feature2;
	}

	public void setFeature2(String feature2) {
		this.feature2 = feature2;
	}

	public String getFeature3() {
		return feature3;
	}

	public void setFeature3(String feature3) {
		this.feature3 = feature3;
	}

	public String getFeature5() {
		return feature5;
	}

	public void setFeature5(String feature5) {
		this.feature5 = feature5;
	}

	@Column(name = "feature2")
    private String feature2;
	
	@Column(name = "feature3")
    private String feature3;
	
	@Column(name = "feature4")
    private String feature5;
	
	@ManyToOne
    private Policy policy;
	
	

	
	
	

}
