package com.example.insurance.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "policy")
public class Policy {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policyId")
    private long policyId;

 

    @Column(name = "policyName")
    private String policyName;

 

    @Column(name = "minEntryAge")
    private String minEntryAge;

 

    @Column(name = "maximumMaturityAge")
    private String maximumMaturityAge;

 

    @Column(name = "minPolicyTerm")
    private String minPolicyTerm;

 

    @Column(name = "MinimumPremium")
    private String minimumPremium;

 

    @Column(name = "MinSumAssured")
    private String minSumAssured;

 

    public String getMinimumPremium() {
		return minimumPremium;
	}



	public void setMinimumPremium(String minimumPremium) {
		this.minimumPremium = minimumPremium;
	}



	public String getMinSumAssured() {
		return minSumAssured;
	}



	public void setMinSumAssured(String minSumAssured) {
		this.minSumAssured = minSumAssured;
	}



	@Column(name = "policyDesc")
    private String policyDesc;

 

    public long getPolicyId() {
        return policyId;
    }

 

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }

 

    public String getPolicyName() {
        return policyName;
    }

 

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

 

    public Policy() {
        super();

 

    }

 

    public String getMinEntryAge() {
        return minEntryAge;
    }

 

    public void setMinEntryAge(String minEntryAge) {
        this.minEntryAge = minEntryAge;
    }

 

    public String getMaximumMaturityAge() {
        return maximumMaturityAge;
    }

 

    public void setMaximumMaturityAge(String maximumMaturityAge) {
        this.maximumMaturityAge = maximumMaturityAge;
    }

 

    public String getMinPolicyTerm() {
        return minPolicyTerm;
    }

 

    public void setMinPolicyTerm(String minPolicyTerm) {
        this.minPolicyTerm = minPolicyTerm;
    }

 

  

 

    public String getPolicyDesc() {
        return policyDesc;
    }

 

    public void setPolicyDesc(String policyDesc) {
        this.policyDesc = policyDesc;
    }

 

    public Policy(long policyId, String policyName, String minEntryAge, String maximumMaturityAge, String minPolicyTerm,
            String minimumPremium, String minSumAssured, String poilicyDesc) {
        super();
        this.policyId = policyId;
        this.policyName = policyName;
        this.minEntryAge = minEntryAge;
        this.maximumMaturityAge = maximumMaturityAge;
        this.minPolicyTerm = minPolicyTerm;
        this.minimumPremium = minimumPremium;
        this.minSumAssured = minSumAssured;
        this.policyDesc=poilicyDesc;

 

    }
    
    
}
