package com.example.insurance.entity;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee_policy")
public class EmployeePolicy {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employeePolicyId")
    private long employeePolicyId;
	
	@Column(name = "employeeId")
    private long employeeId;
	
	@Column(name = "policyId")
    private long policyId;
	
	@Column(name = "subscribedOn")
	private String subscribedOn;
	         
	
	
	


	public String getSubscribedOn() {
		return subscribedOn;
	}



	public void setSubscribedOn(String subscribedOn) {
		this.subscribedOn = subscribedOn;
	}



	public long getEmployeePolicyId() {
		return employeePolicyId;
	}



	public void setEmployeePolicyId(long employeePolicyId) {
		this.employeePolicyId = employeePolicyId;
	}



	public long getEmployeeId() {
		return employeeId;
	}



	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}



	public long getPolicyId() {
		return policyId;
	}



	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}



	public EmployeePolicy(long employeePolicyId, long employeeId, long policyId) {
		super();
		this.employeePolicyId = employeePolicyId;
		this.employeeId = employeeId;
		this.policyId = policyId;
	}



	public EmployeePolicy() {
		super();
		
	}
	
	
	
	

}
